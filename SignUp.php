

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up For New Student</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet"> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>   
</head>
<body>
<?php 
    session_start();
    $nameErr = $genderErr = $majorErr = $dateErr = $imageErr = "";
    $name = $gender = $major = $date = $address = $image = "";
    $_SESSION["name"] = $name;
    $_SESSION['gender'] = $gender;
    $_SESSION['major'] = $major;
    $_SESSION['date'] = $date;
    $_SESSION['name'] = $name;

    $check = false;
    function check () {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["name"])) {
            $nameErr = "Hãy nhập tên";
            } else {
            $name = test_input($_POST["name"]);
            }
            if (empty($_POST["gender"])) {
                $genderErr = "Hãy chọn giới tính";
            } else {
                $gender = test_input($_POST["gender"]);
            }
            if (strlen($_POST["major"])===0) {
                $majorErr = "Hãy chọn phân khoa.";
            } else {
                $major = test_input($_POST["major"]); 
            }
            if (empty($_POST["date"])) {
                $dateErr = "Hãy nhập ngày sinh";
            }else {
                $date = test_input($_POST["date"]);
            }
            if (empty($_POST["image"])) {
                $image = test_input($_POST["image"]);
            }      
        }
        if (isset($name)) {
            return true;
        }
    }
    
    
    
    function validateDate($date, $format = 'd-m-y') {
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
    }

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
?>
<div class="border" style = "border: 2px solid rgb(26, 117, 255);
        background-color: white;
        padding: 20px 30px 20px 30px;
        position: absolute;
        margin-left: 35%;
        margin-top: 5%;
        width: 50%;
        ">
        <span class="error" style="color: red;"> <?php echo $nameErr;?></span><br> <br>
        <span class="error" style="color: red;"> <?php echo $genderErr;?></span> <br> <br>
        <span class="error" style="color: red;"> <?php echo $majorErr;?></span> <br> <br>
        <span class="error" style="color: red;"> <?php echo $dateErr;?></span> <br> <br>
    <form method="post" action="SignUp2.php" >

        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        
                <div style=" margin-left: 15%;" class="form">
                    <div class="name">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)" for="name">Họ và tên <span style="color:red;">*</span></span>
                            <input type="text" name="name" style=" margin-left: 5%;
                            border:2px solid rgb(26, 117, 255);
                            height: 35px;
                            width:50%"
                            />
                        
                    </div>
                    <div style="margin-top:30px">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)" for="name">Giới tính <span style="color:red;">*</span></span> 
                        <?php
                        $gender = array("0"=>"Nam","1"=>"Nữ");
                        for ($i = 0; $i <= 1; $i++) {
                            if($i%2 == 0) {
                            echo '<input value="'.$gender[$i].'" id="'.$gender[$i].'" style="margin-left:30px" type="radio" name="gender">'.$gender[$i];
                            echo '<input value="'.$gender[$i+1].'" id="'.$gender[$i+1].'" style="margin-left:30px" type="radio" name="gender">'.$gender[$i+1];
                            }
                        }
                        ?>            
                    </div>
                    <div class = "subject">
                        <span class="subject_selection" 
                        style="background-color:rgb(0, 230, 0);
                        margin-top : 20px;
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)">Phân khoa <span style="color:red;">*</span></span>
                        <select name="major" id="subjects" style=" margin-left: 5%; height: 40px; border:2px solid rgb(26, 117, 255);">
                        <?php
                            $subject = array(""=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                            foreach($subject as $key => $val){
                                $selected = ($key == ' ') ? 'selected="selected"' : '';
                                echo '<option  value="'. $val .'" ' . $selected . ' >'. $val .'</option>';
                            }
                        ?>
                        </select>
                    <div class="datetime" style="margin-top:30px">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)"for = "date form-control">Ngày Sinh <span style="color:red;">*</span></span>
                        <input class="date" type="text" name="date" style=" margin-left: 5%;
                        border:2px solid rgb(26, 117, 255);
                        height: 35px;
                        width:40%"
                        />
                    </div>
                    <div class="address" style="margin-top : 20px; margin-bottom : 20px">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)" for="address">Địa chỉ</span>
                        <input type="text" name="address" style=" margin-left: 5%;
                        border:2px solid rgb(26, 117, 255);
                        height: 35px;
                        width:60%"
                        />
                        
                    </div>

                    <div class="file" style="margin-top : 20px; margin-bottom : 20px">
                        <span><span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)" for="image">Hình ảnh</span>
                        <input type="file" name="file" style=" margin-left: 5%;"
                        /></span>
                        
                    </div>
                    </div>
                        <div class="sign_up_button">
                            <a href="SignUp2.php" style="background-color:green;
                            color :white;
                            padding :15px 40px;
                            margin-left: 23%;margin-top:40px;
                            border:2px solid rgb(26, 117, 255);
                            border-radius:15px;"> Đăng ký </a>
                        </div>
                </div>
        </form>
    </form>
</div>
</body>
</html>
<script type="text/javascript">  
                        $('.date').datepicker({  
                        format: 'dd-mm-yyyy'  
                        });  
                    </script>  
<style>
    .subject{
        margin-top:30px;
    };
    
</style>
<?php
  
?>