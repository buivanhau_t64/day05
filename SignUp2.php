
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up For New Student</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>   
</head>
<body>
<?php
session_start();
$name = $_POST['name'];
 ?>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <div class="border" style = "border: 2px solid rgb(26, 117, 255);
            background-color: white;
            padding: 20px 30px 20px 30px;
            position: absolute;
            margin-left: 35%;
            margin-top: 5%;
            width: 50%;
            ">
                <div style=" margin-left: 15%;" class="form">
                    <div class="name">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)">Họ và tên <span style="color:red;">*</span></span>
                        <input type="hidden" name="name" value=<?php echo $name?>>
                    </div>
                    <div style="margin-top:30px">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)" for="name">Giới tính <span style="color:red;">*</span></span> 
                        <?php
                        $gender = array("0"=>"Nam","1"=>"Nữ");
                        for ($i = 0; $i <= 1; $i++) {
                            if($i%2 == 0) {
                            echo '<input value="'.$gender[$i].'" id="'.$gender[$i].'" style="margin-left:30px" type="radio" name="gender">'.$gender[$i];
                            echo '<input value="'.$gender[$i+1].'" id="'.$gender[$i+1].'" style="margin-left:30px" type="radio" name="gender">'.$gender[$i+1];
                            }
                        }
                        ?>            
                    </div>
                    <div class = "subject">
                        <span class="subject_selection" 
                        style="background-color:rgb(0, 230, 0);
                        margin-top : 20px;
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)">Phân khoa <span style="color:red;">*</span></span>
                        <select name="major" id="subjects" style=" margin-left: 5%; height: 40px; border:2px solid rgb(26, 117, 255);">
                        <?php
                            $subject = array(""=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                            foreach($subject as $key => $val){
                                $selected = ($key == ' ') ? 'selected="selected"' : '';
                                echo '<option  value="'. $val .'" ' . $selected . ' >'. $val .'</option>';
                            }
                        ?>
                        </select>
                    <div class="datetime" style="margin-top:30px">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)"for = "date form-control">Ngày Sinh <span style="color:red;">*</span></span>
                        <input class="date" type="text" name="date" style=" margin-left: 5%;
                        border:2px solid rgb(26, 117, 255);
                        height: 35px;
                        width:40%"
                        />
                    </div>
                    <div class="address" style="margin-top : 20px; margin-bottom : 20px">
                        <span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)" for="address">Địa chỉ</span>
                        <input type="text" name="address" style=" margin-left: 5%;
                        border:2px solid rgb(26, 117, 255);
                        height: 35px;
                        width:60%"
                        />
                        
                    </div>
                    <div class="file" style="margin-top : 20px; margin-bottom : 20px">
                        <span><span style="background-color:rgb(0, 230, 0);
                        padding : 10px 10px;
                        color :white;
                        border:2px solid rgb(26, 117, 255)" for="image">Hình ảnh</span>
                        <input type="hidden" name="file" style=" margin-left: 5%;"
                        /></span>
                        
                    </div>
                    </div>
                    
                        <div class="sign_up_button">
                        <button style='background-color: #32CD32; border-radius: 5px; 
                                width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Xác nhận</button>
                        </div>
                </div>

    </div>
</form>
</body>
</html>
<script type="text/javascript">  
                        $('.date').datepicker({  
                        format: 'dd-mm-yyyy'  
                        });  
                    </script>  
<style>
    .subject{
        margin-top:30px;
    };
    
</style>
